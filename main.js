const {app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const http = require('http');
const https = require('https');
const fs = require('fs');

const urls = {
  baseUrl: 'https://bitbucket.org/jhonjtoloza/facebook-bot/raw/HEAD',
  scripts: "script-bot.json"
}
let win;
const appDatafolder = app.getPath('userData');

async function createWindow() {
  win = new BrowserWindow({
    width: 1400,
    height: 800,
    backgroundColor: '#ffffff',
    icon: path.join(__dirname, '/dist/assets/logo.png'),
    title: 'FacebookBot',
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    },
  });
  // if (isDev()) {
  if (fs.existsSync(`file://${appDatafolder}/dist/index.html`)) {
    await win.loadURL(`file://${appDatafolder}/dist/index.html`)
  } else {
    await win.loadURL(`file://${__dirname}/dist/index.html`);
  }
  win.on('closed', function () {
    win = null
  });
  checkVersion().then((res) => {
    if (res) {
      installDependencias().then(r => {
      });
    }
  });
}

app.on('ready', createWindow);
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
});
app.on('activate', () => {
  if (win === null) {
    createWindow().then()
  }
});

async function checkVersion() {
  win.webContents.send("checkUpdates", {
    message: "Comprobando actualizaciones.",
  });
  await sleep(2000);
  let responsebody = null;
  let url = `${urls.baseUrl}/${urls.scripts}?v=${new Date().getTime()}`;
  await new Promise(resolve => {
    let resp = https.get(url, (res) => {
      let body = '';
      res.on('data', d => {
        body += d;
      })
      if (res.statusCode === 200) {
        res.on('end', () => {
          responsebody = JSON.parse(body);
          resolve()
        })
      }
    })
    resp.end();
  });
  await sleep(2000);
  let version = '3.0.0';
  let pathfileVersion = `${appDatafolder}/${urls.scripts}`;
  if (fs.existsSync(pathfileVersion)) {
    let content = await readFile(pathfileVersion);
    version = content.version;
  }
  if (version !== responsebody.version) {
    console.log("checkUpdatesMsg enviado");
    win.webContents.send("checkUpdatesMsg", {
      message: "Actualizacion disponible descargando cambios.",
    });
    await sleep(2000)
    responsebody.list.forEach(el => {
      let url = `${urls.baseUrl}/${el}`;
      let path = `${appDatafolder}/${el}`;
      let folder = el.split('/');
      folder.pop();
      checkFolder(appDatafolder, folder.join('/'))
      download(url, path)
    });
    responsebody.listdist.forEach(el => {
      let url = `${urls.baseUrl}/${el}`;
      let path = `${appDatafolder}/${el}`;
      let folder = el.split('/');
      folder.pop();
      checkFolder(appDatafolder, folder.join('/'))
      download(url, path)
    })
    let text = JSON.stringify(responsebody);
    fs.writeFileSync(pathfileVersion, text);
    win.webContents.send("checkUpdatesEnd", {
      message: "Cambios descargados correctamente.",
    });
    if (responsebody.requireNpmInstall || (!fs.existsSync(`${appDatafolder}/scripts-bot/node_modules`))) {
      return true;
    }
  } else {
    win.webContents.send("checkUpdatesEnd", {
      message: "No hay actualizaciones.",
    })
  }
  return false;
}

/**
 * @param targetFile
 * @returns {Promise<String|boolean>}
 */
function readFile(targetFile) {
  return new Promise((resolve) => {
    try {
      fs.readFile(targetFile, function (err, data) {
        if (err) {
          resolve(false);
        }
        if (data !== undefined)
          resolve(JSON.parse(data));
        else
          resolve(false)
      });
    } catch (err) {
      resolve(false);
    }
  })
}

async function download(url, filePath) {
  const proto = !url.charAt(4).localeCompare('s') ? https : http;

  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(filePath);
    let fileInfo = null;

    const request = proto.get(url, response => {
      if (response.statusCode !== 200) {
        reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
        return;
      }

      fileInfo = {
        mime: response.headers['content-type'],
        size: parseInt(response.headers['content-length'], 10),
      };

      response.pipe(file);
    });

    // The destination stream is ended by the time it's called
    file.on('finish', () => resolve(fileInfo));

    request.on('error', err => {
      fs.unlink(filePath, () => reject(err));
    });

    file.on('error', err => {
      fs.unlink(filePath, () => reject(err));
    });

    request.end();
  });
}

function checkFolder(basePath, subfolder) {
  return new Promise(resolve => {
    if (!fs.existsSync(`${basePath}/${subfolder}`)) {
      fs.mkdirSync(`${basePath}/${subfolder}`, {recursive: true});
      resolve()
    } else
      resolve()
  });
}

async function installDependencias() {
  win.webContents.send("checkInstalacion", {
    message: "Instalando dependencias espere...",
  })
  await sleep(2000);
  let {spawn} = require('child_process');
  let npmCmd = require('os').platform().startsWith('win') ? 'npm.cmd' : 'npm';
  let install = spawn(npmCmd, ['install'], {cwd: `${appDatafolder}/scripts-bot`, stdio: 'inherit'});

  install.on('message', message => {
    win.webContents.send("checkInstalacionMsg", {
      message: "Intalacion completa...",
      type: 'success'
    })
  })

  install.on('exit', code => {
    if (code === 0) {
      win.webContents.send("checkInstalacionEnd", {
        message: "Intalacion completa...",
        type: 'success'
      });
      checkCorrectInstall()
    }
  })

  install.on("error", err => {
    win.webContents.send("checkInstalacionEnd", {
      message: "Ocurrio un error instalando chrome" + err,
      type: 'error'
    })
  })

  install.on('disconnect', () => {
    win.webContents.send("checkInstalacionEnd", {
      message: "Se interrumpio la instalacion",
      type: 'error'
    });
  })
}

async function checkCorrectInstall() {
  let command = 'node';
  let params = [`${appDatafolder}/scripts-bot/test.js`];
  let {spawn} = require('child_process');
  let res = spawn(command, params)
  res.on('exit', (code, signal) => {
    if (code !== 0) {
      console.log("Ocurrio un error en la instalacion")
    }
  })
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
