import {Injectable, isDevMode} from '@angular/core';
import {LocalStorage} from '@ngx-pwa/local-storage';
import {ElectronService} from 'ngx-electron';
import {HttpClient} from '@angular/common/http';
import {SnotifyService} from 'ng-snotify';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  public accounts = [];
  fs;

  public account = {
    id: (new Date()).getTime(),
    email: '',
    password: '',
    tested: false,
    groups: [],
    selectedGroups: []
  };
  public pathToBot;
  public pathToFiles;
  public botConfig = {
    showUI: true,
    groupInterval: 0,
  };

  private keys = {
    pathBot: 'pathBot',
    account: 'accounts',
    botConfig: 'bot_config'
  };

  constructor(private storage: LocalStorage,
              private electron: ElectronService,
              private http: HttpClient,
              private notify: SnotifyService) {
    this.fs = this.electron.remote.require('fs');

    this.pathToFiles = this.electron.remote.app.getPath('userData');
    this.pathToBot = `${this.pathToFiles}/scripts-bot`;

    if (isDevMode()) {
      this.pathToBot = '/home/jhon/Documents/facebook-bot/scripts-bot';
    }

    this.storage.getItem(this.keys.account)
      .subscribe((value: any) => {
        if (value != undefined) {
          this.accounts = value;
        }
      });
    this.storage.getItem(this.keys.botConfig)
      .subscribe((value: any) => {
        if (value != undefined) {
          this.botConfig = value;
        }
      });
  }

  saveAcoount() {
    this.account.tested = true;
    this.accounts.push(this.account);
    this.storage.setItem(this.keys.account, this.accounts)
      .subscribe(() => {
        this.account = {
          id: (new Date()).getTime(),
          email: '',
          password: '',
          tested: false,
          groups: [],
          selectedGroups: []
        };
      });
  }

  saveBotConfig() {
    this.storage.setItem(this.keys.botConfig, this.botConfig);
  }


  save() {
    this.storage.setItem(this.keys.account, this.accounts)
      .subscribe();
  }

}
