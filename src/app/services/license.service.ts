import {Injectable} from '@angular/core';
import {StorageMap} from '@ngx-pwa/local-storage';
import * as CryptoJS from 'crypto-js';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LicenseService {


  uniqueId: string;
  licenseCode: any;

  parseDate: Date;
  currentDate: Date;

  constructor(private storage: StorageMap) {
    this.currentDate = new Date();
    this.storage.get('licence_id').subscribe((value: any) => {
      if (value != null) {
        this.uniqueId = value;
      } else {
        this.uniqueId = 'BA_' + LicenseService.makeid();
        this.storage.set('licence_id', this.uniqueId).subscribe();
      }
    });

    this.storage.get('license_code').subscribe((value: any) => {
      console.log('license_coderead = ', value);
      if (value != null) {
        this.licenseCode = value;
        this.decrypLicense();
      } else {
        let fecha = new Date();
        fecha.setHours(fecha.getHours() + 72);
        fecha.setHours(0);
        fecha.setMinutes(0);
        fecha.setSeconds(0);
        let base = 'Licence=' + fecha.getTime();
        this.licenseCode = CryptoJS.AES.encrypt(base, this.uniqueId).toString();
        this.storage.get('license_code', this.licenseCode);
        this.decrypLicense();
      }
    });
  }

  decrypLicense() {
    let decrypted = CryptoJS.AES.decrypt(this.licenseCode, this.uniqueId);
    console.log(decrypted);
    let fecha = decrypted.toString(CryptoJS.enc.Utf8);
    console.log(fecha.split('=').pop());
    this.parseDate = new Date();
    this.parseDate.setTime(parseInt(fecha.split('=').pop()));
    this.parseDate.setHours(0);
    this.parseDate.setMinutes(0);
    this.parseDate.setSeconds(0);
    console.log(this.parseDate);
  }

  checkLicenceCode() {
    let decrypted = CryptoJS.AES.decrypt(this.licenseCode, this.uniqueId);
    console.log(decrypted);
    let fecha = decrypted.toString(CryptoJS.enc.Utf8);
    console.log(fecha.split('=').pop());
    this.parseDate = new Date();
    this.parseDate.setTime(parseInt(fecha.split('=').pop()));
    this.parseDate.setHours(0);
    this.parseDate.setMinutes(0);
    this.parseDate.setSeconds(0);
    console.log(this.parseDate);
    if ((new Date()).getTime() < this.parseDate.getTime()) {
      this.storage.set('license_code', this.licenseCode).subscribe();
      return true;
    }
    return false;
  }

  static makeid() {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  isActive() {
    return (this.currentDate).getTime() <= this.parseDate.getTime();
  }

  getMonth(month) {
    let meses = [
      'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Nobiembre', 'Diciembre'
    ];
    return meses[month];
  }
}
