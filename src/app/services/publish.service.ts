import {Injectable} from '@angular/core';
import {ShellService} from './shell.service';
import {ConfigService} from './config.service';
import {PublicationService} from './publication.service';
import {Consts} from '../shared/consts';
import {SnotifyPosition, SnotifyService, SnotifyToast} from 'ng-snotify';
import {LicenseService} from './license.service';
import {ElectronService} from 'ngx-electron';
import {ChildProcessService} from 'ngx-childprocess';

@Injectable({
  providedIn: 'root'
})
export class PublishService {

  publishingAccounts = [];

  constructor(
    private shell: ShellService,
    private config: ConfigService,
    private publication: PublicationService,
    private notify: SnotifyService,
    private license: LicenseService,
    private childProcessService: ChildProcessService,
    private electron: ElectronService) {
    console.log('isElectronApp ' + childProcessService.isElectronApp);
  }

  initPublications(account) {
    this.publishingAccounts.push(account);
    let notify: SnotifyToast = this.notify.success('El bot esta publicando ....', 'Bot trabajando ...', {
      timeout: 0,
      icon: 'assets/load.svg',
      position: SnotifyPosition.rightBottom
    });

    const groups = JSON.stringify(this.getOnlyIds(account)).replace(new RegExp('"', 'g'), '\\"');
    const publication = JSON.stringify(this.publication.publication).replace(new RegExp('"', 'g'), '\\"');
    this.shell.execute(Consts.publishGroups, {
      id: account.id,
      u: account.email,
      p: account.password,
      ui: this.config.botConfig.showUI,
      gi: this.config.botConfig.groupInterval,
      gs: groups,
      pu: publication,
      path: this.config.pathToFiles
    })
      .then((groupsResult: any) => {
        console.log('Resultado del bot => ', groupsResult);
        this.notify.remove(notify.id);
        this.notify.success('El bot a terminado de publicar', 'Hecho !!', {
          position: SnotifyPosition.centerTop,
          timeout: 0
        });
        this.setResultOfBot(groupsResult)
      }).catch((err) => {
      this.notify.remove(notify.id);
      if (err == 'invalid_license') {
        this.notify.create({
          body: 'Su lincencia expiro el dia' + this.license.parseDate.getDay() + ' de ' +
            this.license.getMonth(this.license.parseDate.getMonth()) + ' Del ' + this.license.parseDate.getFullYear() + 'Renueva para seguir usando',
          title: 'Atención',
          config: {
            position: SnotifyPosition.centerTop,
            timeout: 0,
            buttons: [
              {
                text: 'Renovar',
                action: () => {
                  this.electron.remote.shell.openExternal('http://autopublicadorfacebook.ml/buy');
                },
                bold: true
              }
            ]
          }
        });
      } else {
        this.notify.error('Ocurrio un error ejecutando el bot, contacte al desarrollador');
      }
    });
  }

  getOnlyIds(account) {
    return account.selectedGroups.map(el => {
      return {id: el.id}
    });
  }

  setResultOfBot(result) {
    let account_id = result.account_id;

    let index = this.publishingAccounts.findIndex(el => el.id == account_id)
    const account = this.publishingAccounts[index];
    for (let group of result.groups) {
      for (let localGroup of account.groups) {
        if (group.id == localGroup.id) {
          if (group.publication) {
            localGroup.publications++;
          }
          localGroup.error = group.error;
          break;
        }
      }
    }
    let i = this.config.accounts.findIndex(el => {
      return el.id == account_id
    })
    this.config.accounts[i] = account;
    this.publishingAccounts.splice(index, 1);
    this.config.save();
  }

  isPublish(account) {
    return this.publishingAccounts.find(el => el.id == account.id) > -1;
  }
}
