import {Injectable} from '@angular/core';
import {StorageMap} from '@ngx-pwa/local-storage';
import {ConfigService} from './config.service';
import {ElectronService} from "ngx-electron";

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  public publication = {
    text: '',
    imgs: []
  };
  private keys = {
    publication: 'publication'
  };

  private pathToPhotos = 'publication/';
  fs;

  constructor(private storage: StorageMap,
              private configService: ConfigService,
              private electronService: ElectronService) {
    console.log(this.electronService);
    this.fs = this.electronService.remote.require('fs');
    this.storage.get(this.keys.publication).subscribe((value: any) => {
      if (value != undefined) {
        this.publication.text = value.text;
        this.publication.imgs = value.imgs;
      }
    });
    this.checkFolder().then();
  }

  private checkFolder() {
    return new Promise(resolve => {
      if (!this.fs.existsSync(`${this.configService.pathToFiles}/publication`)) {
        this.fs.mkdir(`${this.configService.pathToFiles}/publication`, () => {
          resolve();
        });
      }
    });
  }

  savePublication() {
    this.storage.set(this.keys.publication, this.publication)
      .subscribe();
  }

  copyImages(filePaths) {
    return new Promise(resolve => {
      filePaths.forEach((item, i) => {
        const ext = PublicationService.getExtension(item);
        let target = `${this.configService.pathToFiles}/${this.pathToPhotos}${PublicationService.generateNum()}.${ext}`;
        console.log(target);
        this.copyFile(item, target)
          .then(() => {
            this.publication.imgs.push(target);
            resolve();
          });
      });
    });
  }

  private copyFile(source, target) {
    const rd = this.fs.createReadStream(source);
    const wr = this.fs.createWriteStream(target);
    return new Promise(function (resolve, reject) {
      rd.on('error', reject);
      wr.on('error', reject);
      wr.on('finish', resolve);
      rd.pipe(wr);
    }).catch(function (error) {
      rd.destroy();
      wr.end();
      throw error;
    });
  }

  private static getExtension(filename: string) {
    return filename.split('.').pop();
  }

  private static generateNum(min = 100, max = 999) {
    return (Math.random() * (max - min) + min).toFixed(0);
  }

  removeImg(i: number) {
    this.publication.imgs.splice(i, 1);
    this.savePublication();
  }
}
