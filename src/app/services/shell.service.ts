import { Injectable } from '@angular/core';
import { ChildProcessService } from 'ngx-childprocess';
import { ConfigService } from './config.service';
import { FsService } from 'ngx-fs';
import { LicenseService } from './license.service';

@Injectable({
  providedIn: 'root'
})
export class ShellService {

  constructor(
    private childProcessService: ChildProcessService,
    private configService: ConfigService,
    private fs: FsService,
    private lisence: LicenseService) {

  }

  execute(command, args = {}, options = []) {
    return new Promise((resolve, reject) => {
      const parsedArgs = [];
      for (const key in args) {
        parsedArgs.push(`--${key} "${args[key]}"`);
      }
      console.log('comman is: ', `node "${this.configService.pathToBot}/${command}" ${parsedArgs.join(' ')}`);
      if (this.lisence.isActive()) {
        this.childProcessService.childProcess
          .exec(`node "${this.configService.pathToBot}/${command}" ${parsedArgs.join(' ')}`, options,
            (err, stdout, stderr) => {
              console.log('Error => ', err);
              console.log('stdout success => ', stdout);
              console.log('stderr error => ', stderr);
              if (err || stderr.length) {
                reject(err);
                this.grabError(err, command);
              } else {
                try {
                  resolve(JSON.parse(stdout));
                } catch (e) {
                  console.log('no se pudo parsear');
                  reject();
                }
              }
            });
      } else {
        reject('invalid_license');
      }
    });
  }

  private async grabError(data, command) {
    const targetFile = `${this.configService.pathToFiles}/errors/error_${command}_${new Date().getTime()}.log`;
    await this.checkFolder();
    return new Promise((resolve, reject) => {
      try {
        this.fs.fs.writeFile(targetFile, data, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve(targetFile);
          }
        });
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  }

  private checkFolder() {
    return new Promise(resolve => {
      if (!this.fs.fs.existsSync(`${this.configService.pathToFiles}/errors`)) {
        this.fs.fs.mkdir(`${this.configService.pathToFiles}/errors`, () => {
          resolve();
        });
      }
    });
  }
}
