import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConfigComponent} from "./components/config/config.component";
import {GroupsComponent} from "./components/groups/groups.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";


const routes: Routes = [
  {path: 'config', component: ConfigComponent},
  {path: 'groups', component: GroupsComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: '**', redirectTo: 'dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
