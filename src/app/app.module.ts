import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {GroupsComponent} from "./components/groups/groups.component";
import {ConfigComponent} from "./components/config/config.component";
import {SanitizeUriPipe} from "./pipe/sanitize-uri.pipe";
import {FormsModule} from "@angular/forms";
import {NgxChildProcessModule} from "ngx-childprocess";
import {NgxElectronModule} from "ngx-electron";
import {SnotifyModule, SnotifyService, ToastDefaults} from "ng-snotify";
import {NgxFsModule} from "ngx-fs";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import {PickerModule} from "@ctrl/ngx-emoji-mart";
import {AlertComponent} from "./shared/alert/alert.component";
import {NavbarComponent} from "./shared/navbar/navbar.component";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    GroupsComponent,
    ConfigComponent,
    SanitizeUriPipe,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxChildProcessModule,
    NgxElectronModule,
    SnotifyModule,
    PickerModule,
    NgxFsModule,
    AngularMultiSelectModule
  ],
  providers: [
    {provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AlertComponent
  ]
})
export class AppModule {
}
