import {Component, OnInit} from '@angular/core';
import {PublishService} from '../../services/publish.service';
import {ConfigService} from '../../services/config.service';
import {SnotifyService} from 'ng-snotify';
import {PublicationService} from '../../services/publication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  currentAccount: any = null;

  constructor(
    public publisBot: PublishService,
    public configService: ConfigService,
    public notify: SnotifyService,
    private publicacion: PublicationService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  initBot() {

    if (this.currentAccount.groups.length) {
      if (this.currentAccount.selectedGroups.length) {
        if (this.publicacion.publication.text.length) {
          this.publisBot.initPublications(this.currentAccount);
        } else {
          this.notify.error('Ingrese un minimo un texto para la publicación', 'Error');
        }
      } else {
        this.notify.error('Seleccione los grupos primero', 'Error');
        this.router.navigate(['/dashborad']).then();
      }
    } else {
      this.notify.error('Aun no ha extraido los grupos de la cuenta');
      this.router.navigate(['/groups']).then();
    }

  }
}
