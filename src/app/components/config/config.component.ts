import { Component } from '@angular/core';
import { ConfigService } from '../../services/config.service';
import { ElectronService } from 'ngx-electron';
import { Consts } from '../../shared/consts';
import { SnotifyPosition, SnotifyService } from 'ng-snotify';
import { ShellService } from '../../services/shell.service';
import { LicenseService } from '../../services/license.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent {

  constructor(
    public configService: ConfigService,
    private electron: ElectronService,
    private shell: ShellService,
    private notify: SnotifyService,
    public license: LicenseService) {
    console.log();
  }

  saveAndTestAccount() {
    this.notify.success('Espere por favor bot trabajando.', 'Cargando ...', {
      timeout: 0,
      icon: 'assets/load.svg',
      position: SnotifyPosition.centerTop
    });
    this.shell.execute(Consts.loginTest, {
      id: this.configService.account.id,
      u: this.configService.account.email,
      p: this.configService.account.password,
      fl: true,
      ui: true,
      path: this.configService.pathToFiles
    })
      .then(value => {
        this.notify.clear();
        console.log(value);
        this.configService.saveAcoount();
        this.notify.success('Configuración de la cuenta parece estar en orden, ahora extraiga los grupos de la cuenta',
          'Felicidades', {timeout: 5000});
      })
      .catch(err => {
        this.notify.clear();
        if (err == 'invalid_license') {
          this.showInvalid();
        } else {
          this.notify.error('La configuración de la cuenta no fue correcta compruebe los datos');
        }
        console.log(err);
      });
  }

  checkLicenceCode() {
    if (this.license.checkLicenceCode()) {
      this.notify.success('Gracias por comprar nuestro autopublicador, su nueva licencia vence el: '
        + this.license.parseDate.getDate() + ' del mes de ' + this.license.getMonth(this.license.parseDate.getMonth()) +
        ' del año ' + this.license.parseDate.getFullYear(), 'Gracias');
    } else {
      this.notify.error('Licencia invalida !!');
    }
  }

  showInvalid() {
    this.notify.create({
      body: 'Su lincencia expiro el dia ' + this.license.parseDate.getDate() + ' del mes de ' +
        this.license.getMonth(this.license.parseDate.getMonth()) + ' Del ' + this.license.parseDate.getFullYear() + 'Renueva para seguir usando',
      title: 'Licencia expirada',
      config: {
        position: SnotifyPosition.centerTop,
        timeout: 0,
        buttons: [
          {
            text: 'Comprar licencia',
            action: () => {
              this.electron.remote.shell.openExternal('http://autopublicadorfacebook.ml/buy');
            },
            bold: true
          }
        ]
      }
    });
  }

  removeAccount(index){
    this.configService.accounts.splice(index, 1);
    this.configService.save();
  }
}
