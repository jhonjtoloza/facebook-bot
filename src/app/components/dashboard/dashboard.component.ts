import {Component, OnInit} from '@angular/core';
import {ElectronService} from 'ngx-electron';
import {PublicationService} from '../../services/publication.service';
import {ConfigService} from '../../services/config.service';
import {SnotifyService} from 'ng-snotify';
import {PublishService} from "../../services/publish.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  i18n = {
    search: 'Buscar',
    notfound: 'No encontrado',
    categories: {
      search: 'Resultados',
      recent: 'Usados recientemente',
      people: 'Caritas y personas',
      nature: 'Animales y naturaleza',
      foods: 'Comidas y bebidas',
      activity: 'Actividades',
      places: 'Viajes y sitios',
      objects: 'Objetos',
      symbols: 'Simbolos',
      flags: 'Banderas',
      custom: 'Otros',
    }
  };

  dropdownSettings = {
    searchAutofocus: true,
    searchPlaceholderText: 'Buscar',
    singleSelection: false,
    text: 'Selecccione los grupos',
    selectAllText: 'Seleccionar todo',
    unSelectAllText: 'Desmarcar todo',
    enableSearchFilter: true,
    labelKey: 'name',
    searchBy: ['name'],
    badgeShowLimit: 20,
    filterSelectAllText: 'Seleccionar todo lo encontrado',
    filterUnSelectAllText: 'Desmarcar todo lo encontrado',
    maxHeight: 500
  };

  currentAccount: any = null;

  constructor(
    private electron: ElectronService,
    public publicationService: PublicationService,
    public configService: ConfigService,
    private notify: SnotifyService,
    public publishService: PublishService) {

  }

  ngOnInit() {

  }

  addEmoji(e) {
    this.publicationService.publication.text += e.emoji.native;
  }

  removeImg(i) {
    this.publicationService.removeImg(i);
  }

  addPhotos() {
    this.electron.remote.dialog.showOpenDialog(null
      , {
        properties: ['multiSelections', 'openFile']
      }).then((filePaths) => {
      this.publicationService.copyImages(filePaths.filePaths)
        .then().catch();
    });
  }

  saveConfig() {
    this.configService.saveBotConfig();
    this.notify.success('Configuración del bot guardada', 'Hecho !!');
  }

  savePublicacion() {
    this.configService.save();
    this.publicationService.savePublication();
    this.notify.success('Grupos seleccionados y publicación guardada.', 'Hecho !!');
  }
}
