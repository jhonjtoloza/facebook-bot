import {Component, OnInit} from '@angular/core';
import {SnotifyPosition, SnotifyService} from 'ng-snotify';
import {Consts} from '../../shared/consts';
import {ShellService} from '../../services/shell.service';
import {ConfigService} from '../../services/config.service';
import {LicenseService} from '../../services/license.service';
import {Router} from '@angular/router';
import {ElectronService} from 'ngx-electron';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  constructor(
    private shell: ShellService,
    private notify: SnotifyService,
    private config: ConfigService,
    private license: LicenseService,
    private route: Router,
    private electron: ElectronService,
    public configService: ConfigService) {

  }

  currentAccount = null;

  ngOnInit() {
  }

  readGroups() {

    this.notify.success('Espere por favor bot trabajando.', 'Cargando ...', {
      timeout: 0,
      icon: 'assets/load.svg',
      position: SnotifyPosition.centerTop
    });
    this.shell.execute(Consts.extractGroups, {
      id: this.currentAccount.id,
      u: this.currentAccount.email,
      p: this.currentAccount.password,
      ui: true,
      path: this.config.pathToFiles
    })
      .then((value: any[]) => {

        this.notify.clear();
        this.currentAccount.groups = value;
        this.configService.accounts = this.configService.accounts.map(el => {
          if (el.id == this.currentAccount.id) {
            el.groups = value;
          }
          return el;
        });
        console.log(this.configService.accounts);
        this.configService.save();


        this.notify.success('Grupos cargado correctamente', 'Hecho !!');
      }).catch(err => {
      if (err == 'invalid_license') {
        this.notify.create({
          body: 'Su lincencia expiro el dia ' + this.license.parseDate.getDate() + ' del mes de ' +
            this.license.getMonth(this.license.parseDate.getMonth()) + ' Del ' + this.license.parseDate.getFullYear() + 'Renueva para seguir usando',
          title: 'Atención',
          config: {
            position: SnotifyPosition.centerTop,
            timeout: 0,
            buttons: [
              {
                text: 'Renovar',
                action: () => {
                  this.electron.remote.shell.openExternal('http://autopublicadorfacebook.ml/buy');
                },
                bold: true
              }
            ]
          }
        });
      }
      this.notify.clear();
      this.notify.error('Ocurrio un error extrayendo los grupos verifique la cuenta en configuración', 'Error');
    });
  }

  setAcount() {

  }
}
