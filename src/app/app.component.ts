import {Component} from '@angular/core';
import {Snotify, SnotifyService} from "ng-snotify";
import {ElectronService} from "ngx-electron";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'facebook-bot-app';
  ipc;

  constructor(private snotify: SnotifyService,
              private electron: ElectronService) {
    this.ipc = this.electron.ipcRenderer;

    this.ipc.on('checkUpdates', (event, data) => {
      this.snotify.async(data.message, this.awaitEndUpdates());
    });

    this.ipc.on('checkInstalacion', (event, data) => {
      this.snotify.async(data.message, this.awaitEndInstalacion());
    })
  }

  awaitEndUpdates(): Promise<Snotify> {
    return new Promise(resolve => {
      this.ipc.on('checkUpdatesEnd', (event, data) => {
        resolve({
          body: data.message,
          config: {
            timeout: 3000,
            closeOnClick: true,
            type: 'success'
          }
        })
      })
    })
  }

  awaitEndInstalacion(): Promise<Snotify> {
    return new Promise((resolve) => {
      this.ipc.on('checkInstalacionEnd', (event, data) => {
        resolve({
          body: data.message,
          config: {
            timeout: 3000,
            type: data.type,
            closeOnClick: true
          }
        })
      })
    })
  }
}
