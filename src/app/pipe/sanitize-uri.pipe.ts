import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'sanitizeUri'
})
export class SanitizeUriPipe implements PipeTransform {

  constructor(private sanitize: DomSanitizer) {
  }

  transform(value: any, args?: any): any {
    return this.sanitize.bypassSecurityTrustResourceUrl(value);
  }

}
