const init = require('./init');

(async () => {
  const args = require('minimist')(process.argv.slice(2));
  init.getBrowserWithLogin(args.id, args.u, args.p, args.path , args.fl === 'true', args.ui === 'true')
    .then(async value => {
      process.stdout.write(JSON.stringify({success: 'ok', isNewVersion: value.isNewVersion}));
      await value.browser.close();
    }).catch(async (err) => {
    console.log(err);
    throw new Error('Ocurrio un error al login de usuario verifique usuario y contraseña - ' + err);
  });
})();
