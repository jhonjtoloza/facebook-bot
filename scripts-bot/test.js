let puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: {
      width: 1350,
      height: 800
    }
  });
  const page = await browser.newPage();
  await page.goto('https://google.com');
  await page.waitForNavigation();
  await browser.close();
  console.log(JSON.stringify({
    success: 'ok'
  }))
})();
