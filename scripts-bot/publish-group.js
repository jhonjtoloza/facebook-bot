const init = require('./init');
const conts = require('./common/consts');
const helper = require('./common/helper');
const clipboardy = require('clipboardy');


(async () => {
  const wait = (ms) => new Promise(resolve => setTimeout(resolve, ms))
  const args = require('minimist')(process.argv.slice(2));
  init.getBrowserWithLogin(args.id, args.u, args.p, args.path, args.fl === 'true', args.ui === 'true')
    .then(async value => {
      let page = value.page;
      let browser = value.browser;
      const isNewVersion = value.isNewVersion;
      let groups = JSON.parse(args.gs);
      let publlish = JSON.parse(args.pu);
      if (!isNewVersion) {
        for (let group of groups) {
          try {
            await page.goto(conts.LINKS.BASE_GROUP + group.id, {timeout: 0});
            await page.waitForSelector(conts.SELECTORS.GROUP_PUBLIS_SECTION, {timeout: 10000});
            if (await page.$(conts.SELECTORS.GROUP_TEXTAREA_SECTION) !== null) {
              await page.evaluate(_ => {
                window.scrollBy(0, 0);
              });
              await page.click(conts.SELECTORS.GROUP_TEXTAREA_SECTION);
              await page.waitForSelector(conts.SELECTORS.GROUP_TEXTAREA_SECTION, {hidden: true})
            } else if (await page.$(conts.SELECTORS.GROUP_SELL_INPUT) != null) {
              await page.evaluate(_ => {
                window.scrollBy(0, 0);
              });
              await page.click(conts.SELECTORS.GROUP_SELL_TEXTAREA);
              await page.waitForSelector(conts.SELECTORS.GROUP_HIDESELL, {hidden: true});
              await page.waitForSelector(conts.SELECTORS.GROUP_WAIT_INPUT, {timeout: 0});
              await page.click(conts.SELECTORS.GROUP_WAIT_INPUT);
            } else {
              break;
            }

            const text = init.getTextBySpintax(publlish.text);
            clipboardy.writeSync(text);
            if (clipboardy.readSync() !== null) {
              await page.keyboard.down("ControlLeft");
              await page.keyboard.press("V");
              await page.keyboard.up("ControlLeft");
            } else {
              await page.keyboard.write(text);
            }

            await page.waitForSelector(conts.SELECTORS.GROUP_INPUT_IMGS, {timeout: 0});
            if (publlish.imgs.length) {
              const input = await page.$(conts.SELECTORS.GROUP_INPUT_IMGS);
              input.uploadFile(...publlish.imgs);
              await page.waitForSelector('#pagelet_group_composer div[class="fbScrollableArea"]', {timeout: 0})
            }
            await page.waitForSelector(conts.SELECTORS.GROUP_BTN_PUBLISH, {timeout: 0});
            await page.click(conts.SELECTORS.GROUP_BTN_PUBLISH);
            await page.waitForSelector(conts.SELECTORS.GROUP_COMPLETE_POST, {timeout: 0});

            group.publication = true;
            group.error = false;
            await page.waitForSelector(10000);
          } catch (e) {
            group.error = true;
          }
        }
      } else {
        await page.goto(conts.LINKS.BASE_GROUP, {timeout: 0});
        await page.waitForSelector(conts.SELECTORS.NEW_GROUP_LOAD_MORE);
        let url = page.url().split('?')[0];
        url = url.replace('feed/', '');
        for (const group of groups) {
          try {
            let buldLink = url + group.id;
            let linkOnDom = await page.$(`div[role="navigation"] a[href="${buldLink}/"]`);
            let scrollIsPresent = await page.$('div[role="navigation"] > div div[role="progressbar"]')
            while (linkOnDom === null && scrollIsPresent != null) {
              await page.evaluate(_ => {
                let el = document.querySelector('div[role="navigation"] > div div[role="progressbar"]');
                if (el !== null) {
                  el.scrollIntoView()
                }
              });
              linkOnDom = await page.$(`div[role="navigation"] a[href="${buldLink}/"]`);
              scrollIsPresent = await page.$('div[role="navigation"] > div div[role="progressbar"]')
            }
            if (linkOnDom != null) {
              await page.click(`div[role="navigation"] a[href="${buldLink}/"]`);
              let linkTocheckLoad = `div[role="tablist"] a[href="/groups/${group.id}/about/"`
              //se chequea que div del grupo sea cargado en el dom
              await page.waitForSelector(linkTocheckLoad, {timeout: 25000});
              await wait(3000);
              let link = `div[role="tablist"] a[href="/groups/${group.id}/buy_sell_discussion/"`
              if (await page.$(link) !== null) {
                //se hace clien en "Conversacion" para crear la publicacion
                await page.click(link);
              }

              await page.waitForSelector(conts.SELECTORS.NEW_GROUP_INIT_COMPOSE,{timeout: 25000});
              await wait(3000);
              await page.click(conts.SELECTORS.NEW_GROUP_CLICK_INIT_COMPOSE);
              await wait(3000);
              await page.waitForSelector(conts.SELECTORS.NEW_GROUP_COMPOSE_EL, {timeout: 25000});
              await wait(3000);
              await page.click(conts.SELECTORS.NEW_GROUP_COMPOSE_EL);
              await wait(3000);
              const text = init.getTextBySpintax(publlish.text);
              clipboardy.writeSync(text);
              if (clipboardy.readSync() != null){
                await page.keyboard.down("ControlLeft");
                await page.keyboard.press("V");
                await page.keyboard.up("ControlLeft");
              } else {
                await page.keyboard.type(text);
              }

              if (publlish.imgs.length) {
                const input = await page.$(conts.SELECTORS.NEW_GROUP_FILEINPUT);
                await input.uploadFile(...publlish.imgs);
              }

              await page.waitForSelector(conts.SELECTORS.NEW_GROUP_BTN_PUBLISH, {timeout: 25000});
              await wait(3000);
              await page.click(conts.SELECTORS.NEW_GROUP_BTN_PUBLISH);
              await wait(3000);
              await page.waitForSelector(conts.SELECTORS.NEW_GROUP_END_PUBLISH, {hidden: true, timeout: 0});
              await wait(5000);
              group.publication = true;
            }
          } catch (e) {
            group.error = true;
          }
        }
      }
      process.stdout.write(JSON.stringify({
        account_id: args.id,
        groups: groups
      }));
      await browser.close();
    }).catch(async (e) => {
    process.exit();
    throw new Error('Ocurrio un error al login de usuario verifique usuario y contraseña');
  });
})();
