const init = require('./init');
const conts = require('./common/consts');
const helper = require('./common/helper');

(async () => {
  const args = require('minimist')(process.argv.slice(2));
  init.getBrowserWithLogin(args.id, args.u, args.p, args.path, args.fl === 'true', args.ui === 'true')
    .then(async value => {
      let page = value.page;
      let browser = value.browser;
      const isNewVersion = value.isNewVersion;

      const links = [];
      if (!isNewVersion) {
        let baseUrlGroups = await page.$eval('[data-click="profile_icon"] > a', a => a.getAttribute('href'));
        await page.goto(baseUrlGroups + conts.LINKS.GROUP_LIST, {timeout: 0});

        await page.waitForSelector(conts.SELECTORS.GROUP_SECTION);
        while (await page.$(conts.SELECTORS.GROUP_BTN_MORE) !== null) {
          await page.evaluate(_ => {
            window.scrollBy(0, window.innerHeight);
          });
        }
        const bodyGroupsMember = await page.$eval(conts.SELECTORS.GROUP_SECTION, e => e.outerHTML);
        const $ = helper.getJQueryDom(bodyGroupsMember);

        $('li').each(function (i, element) {
          const el = $(element);
          links.push({
            id: el.find('div[data-collection-item]').attr('data-collection-item').split('::').pop(),
            name: el.find('a:not([class])').text().trim(),
            img: el.find('img').attr('src'),
            publications: 0,
            error: false
          });
        });
      } else {
        await page.goto(conts.LINKS.HOME + conts.LINKS.ME_GROUPS, {timeout: 0});
        await page.waitForSelector(conts.SELECTORS.NEW_GROUP_SECTION, {timeout: 0});

        while (await page.$(conts.SELECTORS.NEW_GROUP_LOAD_MORE) !== null){
          await page.evaluate(_ => {
            window.scrollBy(0, window.innerHeight)
          })
        }
        const bodyGroupsMember = await page.$eval(conts.SELECTORS.NEW_GROUP_SECTION, e => e.outerHTML);
        const $ = helper.getJQueryDom(bodyGroupsMember);

        $('div:nth-child(3) > div').each((i, element) => {
          const el = $(element);
          let linkAndPic = el.find('div:first-child > a')
          if (linkAndPic.attr('href') !== undefined){
            links.push({
              id: linkAndPic.attr('href').split('/').filter(el => el.length !== 0).pop(),
              name: el.find('div:nth-child(2) a > span').text(),
              img: linkAndPic.find('img').attr('src'),
              members: el.find('div:nth-child(2) div:nth-child(2) > span > div > div:nth-child(1)').text(),
              publications: 0,
              error: false
            });
          }
        })
      }
      await helper.checkFolder(args.path, 'groups');
      for (const link of links) {
        link.img = await helper.downloadFile(link.img, `${args.path}/groups/${link.id}.jpg`);
      }
      process.stdout.write(JSON.stringify(links));
      await browser.close();
    }).catch(async (err) => {
    console.log(err);
    throw new Error('Ocurrio un error al login de usuario verifique usuario y contraseña - ' + err);
  });
})();
