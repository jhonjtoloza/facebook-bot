const puppeteer = require('puppeteer');
const helper = require('./common/helper');
const Consts = require('./common/consts');

module.exports = {
  getBrowserWithLogin(id, email, password, basePath, forceLogin = false, showUI = false) {
    return new Promise(async (resolve, reject) => {
      try {
        const browser = await puppeteer.launch({
          args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
          ],
          headless: !showUI,
          defaultViewport: null,
          //executablePath: '/usr/bin/google-chrome' //colocar aqui el link a google chrome
        });
        let context = browser.defaultBrowserContext();
        await context.overridePermissions(Consts.LINKS.HOME, ['geolocation', 'notifications']);
        const pages = await browser.pages();
        let page;
        if (pages.length){
          page = pages[0];
        } else {
          page = await browser.newPage();
        }
        if (!forceLogin) {
          let cookies = await helper.getCookie(`${basePath}/session_${id}.json`);
          if (cookies !== false) {
            await page.setCookie(...cookies);
          }
        }
        await page.goto(Consts.LINKS.HOME, {timeout: 0});
        let navBar = await page.$(Consts.SELECTORS.NAVBAR_IN_LOGIN);
        let newNavBar = await page.$(`[id^="${Consts.SELECTORS.NEW_CHECK_IS_LOGIN}"]`);
        if (navBar !== null || newNavBar !== null) {
          resolve({browser: browser, page: page, isNewVersion: newNavBar !== null});
        } else if (await page.$(Consts.SELECTORS.LOGIN_BTN) != null) {
          await page.click(Consts.SELECTORS.EMAIL);
          await page.keyboard.type(String(email));
          await page.click(Consts.SELECTORS.PASS);
          await page.keyboard.type(String(password));
          await page.click(Consts.SELECTORS.LOGIN_BTN);
          await page.waitForNavigation({waitUntil: 'load'})
          let navBar = await page.$(Consts.SELECTORS.NAVBAR_IN_LOGIN);
          let newNavBar = await page.$(`[id^="${Consts.SELECTORS.NEW_CHECK_IS_LOGIN}"]`);
          if (navBar !== null || newNavBar !== null) {
            const cookies = await page.cookies();
            await helper.saveCookie(cookies, `${basePath}/session_${id}.json`);
            resolve({browser: browser, page: page, isNewVersion: newNavBar !== null})
          } else {
            await browser.close();
            reject("login no exitoso")
          }
        } else {
          await browser.close();
          reject("no se encontro el boton")
        }
      } catch (e) {
        reject(e)
      }
    })
  },
  getTextBySpintax(str) {
    const regex = /\(.*?\)/gm;
    let m;
    let publication = str;
    do {
      m = regex.exec(str);
      if (m) {
        const parts = m[0].replace('(', '').replace(')', '').split('|');
        publication = publication.replace(m[0], parts[Math.floor(Math.random() * parts.length)])
      }
    } while (m);
    return publication;
  }
};
