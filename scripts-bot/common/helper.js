const fs = require('fs');
const request = require('request');
const path = require('path');

module.exports = {
  saveCookie: function (jsonObj, targetFile) {
    return new Promise((resolve, reject) => {

      try {
        const data = JSON.stringify(jsonObj);
        fs.writeFile(targetFile, data, (err, text) => {
          if (err)
            reject(err);
          else {
            resolve(targetFile);
          }
        });
      }
      catch (err) {
        reject(err);
      }
    });
  },

  getCookie: function (targetFile) {
    return new Promise((resolve) => {
      try {
        fs.readFile(targetFile, function (err, data) {
          if (err) {
            resolve(false);
          }
          if (data !== undefined)
            resolve(JSON.parse(data));
          else
            resolve(false)
        });
      }
      catch (err) {
        resolve(false);
      }
    })
  },

  downloadFile(uri, filename) {
    return new Promise(resolve => {
      if (!fs.existsSync(filename)) {
        request.head(uri, function (err, res, body) {
          if (res.statusCode === 200){
            request(uri).pipe(fs.createWriteStream(filename)).on('close', () => {
              resolve(path.resolve(filename));
            });
          } else {
            resolve(null);
          }
        });
      } else {
        resolve(path.resolve(filename));
      }
    });
  },


  serializeUrl: function (obj, prefix) {
    let str = [],
      p;
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        let k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        str.push((v !== null && typeof v === "object") ?
          this.serializeUrl(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
  },

  getJQueryDom(bodyHTML) {
    const jsdom = require("jsdom");
    const {JSDOM} = jsdom;
    const dom = new JSDOM(bodyHTML);
    return (require('jquery'))(dom.window);
  },

  chunkArray(array, chunkSize) {
    return [].concat.apply([],
      array.map(function (elem, i) {
        return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
      })
    );
  },

  checkFolder(basePath, subfolder) {
    return new Promise(resolve => {
      if (!fs.existsSync(`${basePath}/${subfolder}`)) {
        fs.mkdir(`${basePath}/${subfolder}`, () => {
          resolve();
        });
      } else
        resolve()
    });
  }
};
