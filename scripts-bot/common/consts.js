module.exports = {
  SELECTORS: {
    EMAIL: '#email',
    PASS: '#pass',
    LOGIN_BTN: 'button[data-testid="royal_login_button"]',

    GROUP_FAVORITES: '#GroupDiscoverCard_favorite',
    GROUP_SECTION: '#pagelet_timeline_medley_groups',
    GROUP_BTN_MORE: '#pagelet_timeline_medley_groups > div[id] > div > img',

    NEW_GROUP_LOAD_MORE: 'div[role="main"] div[role="progressbar"]',
    NEW_GROUP_SECTION: 'div[role=\'main\'] > div:nth-child(4) > div > div > div > div > div > div > div',

    NAVBAR_IN_LOGIN: '#bluebarRoot',
    NEW_CHECK_IS_LOGIN: 'mount_0',

    GROUP_HIDESELL: '#pagelet_group_composer div[data-test-actorid] div[role="presentation"] label',
    GROUP_WAIT_INPUT: '#pagelet_group_composer div[data-test-actorid] div[role="presentation"] div[aria-autocomplete]',
    GROUP_BTN_PUBLISH: '#pagelet_group_composer button[type=submit]:not([disabled])',
    GROUP_PUBLIS_SECTION: '#pagelet_group_composer div[role="tablist"]',
    GROUP_TEXTAREA_SECTION: '#pagelet_group_composer textarea[name="xhpc_message_text"]',
    GROUP_SELL_INPUT: 'div[role="tablist"] a[attachmentid="SELL"]',
    GROUP_SELL_TEXTAREA: '#pagelet_group_composer > div > div > div:nth-child(2) ul > li:nth-child(2) > a',
    GROUP_INPUT_IMGS: '#pagelet_group_composer a input[type="file"]',
    GROUP_COMPLETE_POST: 'div.composerPostSection > div',

    NEW_GROUP_IS_LOAD: 'div[data-pagelet="GroupInlineComposer"]',
    NEW_GROUP_LINK_IS_BUY: 'div[role="tablist"] > div > a:nth-child(6)[href]',
    NEW_GROUP_INIT_COMPOSE: 'div[data-pagelet="GroupInlineComposer"] > div > div > div> div:nth-child(1) > div > div > span',
    NEW_GROUP_CLICK_INIT_COMPOSE: 'div[data-pagelet="GroupInlineComposer"] > div > div > div> div:nth-child(1) > div',
    NEW_GROUP_COMPOSE_EL: 'div[role="dialog"] form[method="post"] > div> div > div > div > div > div > div:nth-child(2) > div > div > div',
    NEW_GROUP_FILEINPUT: 'div[role="dialog"]  form[method="post"] input[type=file]',
    NEW_GROUP_BTN_PUBLISH: 'div[role="dialog"] form[method="post"]  div[aria-label="Publicar"]:not([aria-disabled])',
    NEW_GROUP_END_PUBLISH: 'div[role="dialog"] form[method="post"]'
  },

  LINKS: {
    HOME: 'https://www.facebook.com',
    BASE_GROUP: 'https://www.facebook.com/groups/',
    GROUP_LIST: '/groups/',
    ME_GROUPS: '/me/groups'
  },
};
